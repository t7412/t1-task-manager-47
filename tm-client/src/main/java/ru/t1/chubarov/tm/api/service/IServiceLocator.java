package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    IPropertyService getPropertyService();

    @Nullable
    ICommandService getCommandService();

    @Nullable
    ILoggerService getLoggerService();

    @Nullable
    IDomainEndpoint getDomainEndpoint();

    @Nullable
    IAuthEndpoint getAuthEndpoint();

    @Nullable
    IProjectEndpoint getProjectEndpoint();

    @Nullable
    ITaskEndpoint getTaskEndpoint();

    @Nullable
    IUserEndpoint getUserEndpoint();

    @NotNull
    ITokenService getTokenService();

    @Nullable
    ISystemEndpoint getSystemEndpoint();

}
