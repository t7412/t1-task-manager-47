package ru.t1.chubarov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.dto.model.SessionDTO;
import ru.t1.chubarov.tm.repository.dto.SessionDtoRepository;
import ru.t1.chubarov.tm.service.ConnectionService;
import ru.t1.chubarov.tm.service.PropertyService;


import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 2;

    @NotNull
    private final String userIdFirstSession = UUID.randomUUID().toString();

    @NotNull
    private final String userIdSecondSession = UUID.randomUUID().toString();

    @NotNull
    private final String firstSessionId = UUID.randomUUID().toString();

    @NotNull
    private List<SessionDTO> sessionList;

    @NotNull
    final IDatabaseProperty databaseProperty = new PropertyService();

    @NotNull
    final IConnectionService connectionService = new ConnectionService(databaseProperty);

    @NotNull
    final EntityManager entityManager = connectionService.getEntityManager();

    @NotNull
    final ISessionDtoRepository sessionRepository = new SessionDtoRepository(entityManager);

    @SneakyThrows
    @Before
    public void initRepository() {
        entityManager.getTransaction().begin();
        sessionList = new ArrayList<>();
        @NotNull final SessionDTO session1 = new SessionDTO();
        session1.setUserId(userIdFirstSession);
        session1.setDate(new Date());
        session1.setId(firstSessionId);
        sessionRepository.add(session1);
        sessionList.add(session1);
        @NotNull final SessionDTO session2 = new SessionDTO();
        session2.setUserId(userIdSecondSession);
        session2.setDate(new Date());
        sessionRepository.add(session2);
        sessionList.add(session2);
        entityManager.getTransaction().commit();
    }

    @After
    public void finish() throws Exception {
        sessionList.clear();
        entityManager.getTransaction().begin();
        sessionRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @SneakyThrows
    @Test
    public void testAdd() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
        @NotNull final SessionDTO session1 = new SessionDTO();
        session1.setUserId(userIdFirstSession);
        session1.setDate(new Date());
        sessionRepository.add(session1);
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, sessionRepository.getSize());
        @NotNull final SessionDTO session2 = new SessionDTO();
        session2.setUserId(userIdFirstSession);
        session2.setDate(new Date());
        sessionRepository.add(session2);
        Assert.assertEquals(NUMBER_OF_ENTRIES + 2, sessionRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemove() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
        @NotNull final SessionDTO session =sessionRepository.findOneById(firstSessionId);
        sessionRepository.remove(session);
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemoveOneById() {
        sessionRepository.removeOneById(userIdFirstSession, "empty_sessionId");
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
        sessionRepository.removeOneById(userIdFirstSession, firstSessionId);
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testExistsById() {
        Assert.assertTrue(sessionRepository.existsById(userIdFirstSession, firstSessionId));
        Assert.assertTrue(sessionRepository.existsById(userIdSecondSession, sessionList.get(1).getId()));
        Assert.assertFalse(sessionRepository.existsById(userIdFirstSession,"empty_id"));
    }

    @SneakyThrows
    @Test
    public void testFindOneById() {
        Assert.assertNotNull(sessionRepository.findOneById(firstSessionId));
    }

    @SneakyThrows
    @Test
    public void testFindOneByIdNegative() {
        Assert.assertNull(sessionRepository.findOneById( "empty_id"));
    }

    @Test
    public void testFindOAll() throws Exception {
        @NotNull final List<SessionDTO> SessionList = sessionRepository.findAll();
        Assert.assertEquals(2, SessionList.size());
        @NotNull final List<SessionDTO> actualSessionList = sessionRepository.findAllByUser(userIdFirstSession);
        Assert.assertEquals(1, actualSessionList.size());
    }

    @SneakyThrows
    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(1, sessionRepository.getSizeByUser(userIdFirstSession));
    }

}
