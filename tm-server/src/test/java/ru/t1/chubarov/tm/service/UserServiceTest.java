package ru.t1.chubarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.service.*;
import ru.t1.chubarov.tm.api.service.dto.IProjectDtoService;
import ru.t1.chubarov.tm.api.service.dto.ITaskDtoService;
import ru.t1.chubarov.tm.api.service.dto.IUserDtoService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.dto.model.UserDTO;
import ru.t1.chubarov.tm.service.dto.ProjectDtoService;
import ru.t1.chubarov.tm.service.dto.TaskDtoService;
import ru.t1.chubarov.tm.service.dto.UserDtoService;
import ru.t1.chubarov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

@Category(UnitCategory.class)
public class UserServiceTest {

    private static final int NUMBER_OF_ENTRIES = 3;

    @NotNull
    private IUserDtoService userService;

    @NotNull
    private IPropertyService propertyService;

    @NotNull
    private List<UserDTO> userList;

    @NotNull
    final IDatabaseProperty databaseProperty = new PropertyService();

    @NotNull
    final IConnectionService connectionService = new ConnectionService(databaseProperty);

    @Before
    public void initTest() throws Exception {
        propertyService = new PropertyService();
        ITaskDtoService taskService = new TaskDtoService(connectionService);
        IProjectDtoService projectService = new ProjectDtoService(connectionService);
        userService = new UserDtoService(projectService, taskService, propertyService, connectionService);

        @NotNull UserDTO admin;
        @NotNull UserDTO user;
        @NotNull UserDTO cat;
        if (userService.isLoginExist("admin")) {
            admin = userService.findByLogin("admin");
        } else {
            admin = userService.create("admin", "admin", Role.ADMIN);
        }
        if (userService.isLoginExist("user")) {
            user = userService.findByLogin("user");
        } else {
            user = userService.create("user", "user", "user@emal.ru");
        }
        if (userService.isLoginExist("cat")) {
            cat = userService.findByLogin("cat");
        } else {
            cat = userService.create("cat", "cat", "car@catof.org");
        }
        userList = new ArrayList<>();
        userList.add(admin);
        userList.add(user);
        cat.setLocked(true);
        userService.lockUserByLogin("cat");
        userList.add(cat);
    }

    @After
    public void finish() throws Exception {
        userService.removeByLogin("test");
        userService.removeByLogin("admin");
        userService.removeByLogin("user");
        userService.removeByLogin("cat");
        userService.removeByLogin("mouse");
        userService.removeByLogin("dog");
        userService.removeByLogin("bear");
        userService.removeByLogin("raccoon");
    }

    @Test
    public void testSize() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES, userService.getSize());
    }

    @Test
    public void testCreate() throws Exception {
        userService.create("dog", "dog", Role.ADMIN);
        userService.create("bear", "bear", "bear@emal.ru");
        userService.create("raccoon", "raccoon");
        Assert.assertEquals(NUMBER_OF_ENTRIES + 3, userService.getSize());
    }

    @Test
    public void testCreateNegative() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("","",""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null,"dog","dog@mail.org"));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create("dog","",""));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create("dog",null,"dog@mail.org"));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create("user","user","user@emal.ru"));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create("user","user",""));
    }

    @Test
    public void testFindByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
        Assert.assertNull(userService.findByLogin("admin").getEmail());
        Assert.assertEquals(Role.ADMIN, userService.findByLogin("admin").getRole());
    }

    @Test
    public void testFindByEmail() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
        Assert.assertEquals("admin", userService.findByLogin("admin").getLogin());
    }

    @Test
    public void testClear() throws Exception {
        userService.clear();
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveOne() throws Exception {
        userService.removeOne(userList.get(1));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, userService.getSize());
    }

    @Test
    public void testRemoveByLogin() throws Exception {
        userService.removeByLogin("user");
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, userService.getSize());
        try {
            userService.findByLogin("user").getEmail();
        } catch (Exception e) {
            Assert.assertNull(e.getMessage());
        }
    }

    @Test
    public void testRemoveByEmail() throws Exception {
        userService.removeByEmail("user@emal.ru");
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, userService.getSize());
        try {
            userService.findByLogin("user").getEmail();
        } catch (Exception e) {
            Assert.assertNull(e.getMessage());
        }
    }

    @Test
    public void testRemoveNegative() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(""));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(""));
        Assert.assertNull(userService.removeOne(null));
    }

    @Test
    public void testUnlock() throws Exception {
        Assert.assertTrue(userService.findByLogin("cat").getLocked());
        userService.unlockUserByLogin("cat");
        Assert.assertFalse(userService.findByLogin("cat").getLocked());
    }

    @Test
    public void testLock() throws Exception {
        Assert.assertFalse(userService.findByLogin("user").getLocked());
        userService.lockUserByLogin("user");
        Assert.assertTrue(userService.findByLogin("user").getLocked());
    }

    @Test
    public void testUpdateUser() throws Exception {
        userService.updateUser(userService.findByLogin("user").getId(), "Ivanov", "Peter", "Sidorovich");
        Assert.assertEquals("Ivanov", userService.findByLogin("user").getFirstName());
        Assert.assertEquals("Peter", userService.findByLogin("user").getLastName());
        Assert.assertEquals("Sidorovich", userService.findByLogin("user").getMiddleName());
    }

    @Test
    public void testIsEmailExist() throws Exception {
        Assert.assertFalse(userService.isEmailExist(null));
        Assert.assertFalse(userService.isEmailExist("user@1.s"));
        Assert.assertTrue(userService.isEmailExist("user@emal.ru"));
    }

    @Test
    public void testIsLoginExist() throws Exception {
        Assert.assertFalse(userService.isLoginExist(null));
        Assert.assertFalse(userService.isLoginExist("fail_login"));
        Assert.assertTrue(userService.isLoginExist("user"));
    }


    @Test
    public void testSetPassword() throws Exception {
        @NotNull final String userId = userService.findByLogin("user").getId();
        userService.setPassword(userId,"user_new_pas");
        Assert.assertEquals(HashUtil.salt(propertyService, "user_new_pas"), userService.findByLogin("user").getPasswordHash());
    }

}
