package ru.t1.chubarov.tm.service;

import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.dto.model.ProjectDTO;
import ru.t1.chubarov.tm.dto.model.SessionDTO;
import ru.t1.chubarov.tm.dto.model.TaskDTO;
import ru.t1.chubarov.tm.dto.model.UserDTO;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.model.Session;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperty;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.entityManagerFactory = factory();
    }

    @Override
    @SneakyThrows
    @NotNull
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    public EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(org.hibernate.cfg.Environment.URL, databaseProperty.getDatabaseUrl());
        settings.put(org.hibernate.cfg.Environment.USER, databaseProperty.getDatabaseUsername());
        settings.put(org.hibernate.cfg.Environment.PASS, databaseProperty.getDatabasePassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseHbm2DDL_Auto());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, databaseProperty.getDatabaseShowSql());

        settings.put(org.hibernate.cfg.Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getDatabaseUseSecond());
        settings.put(org.hibernate.cfg.Environment.USE_QUERY_CACHE, databaseProperty.getDatabaseUseQueryCache());
        settings.put(org.hibernate.cfg.Environment.USE_MINIMAL_PUTS, databaseProperty.getDatabaseMinimalPuts());
        settings.put(org.hibernate.cfg.Environment.CACHE_REGION_PREFIX, databaseProperty.getDatabaseRegionPrefix());
        settings.put(org.hibernate.cfg.Environment.CACHE_REGION_FACTORY, databaseProperty.getDatabaseRegionFactory());
        settings.put(org.hibernate.cfg.Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getDatabaseProviderConfig());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
