package ru.t1.chubarov.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.model.IProjectModelRepository;
import ru.t1.chubarov.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectModelRepository {

    public ProjectRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        return entityManager
                .createQuery("SELECT p FROM Project p", Project.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Project> findAllByUser(@Nullable String userId) {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.user.id = :userId", Project.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull String id) {
        return entityManager
                .find(Project.class, id);
    }

    @Nullable
    @Override
    public Project findOneByIdByUser(@Nullable String userId, @Nullable String id) {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.user.id = :userId AND p.id = :id", Project.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setFirstResult(1)
                .getResultList().stream().findFirst().orElse(null);
    }


    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Project").executeUpdate();
    }

    @Override
    public void removeAll(@Nullable String userId) {
        entityManager
                .createQuery("DELETE FROM Project p WHERE p.user.id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void remove(@Nullable String userId, @NotNull Project model) {
        entityManager
                .createQuery("DELETE FROM Project p WHERE p.user.id = :userId and p.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", model.getId())
                .executeUpdate();
    }

    @Override
    public void removeOneById(@Nullable String userId, @Nullable String id) {
        entityManager
                .createQuery("DELETE FROM Project p WHERE p.user.id = :userId and p.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM Project p", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public long getSizeByUser(@Nullable String userId) {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM Project p WHERE p.user.id = :userId", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Boolean existsById(@Nullable String userId, @Nullable String id) {
        return entityManager
                .createQuery("SELECT COUNT(p) = 1 FROM Project p WHERE p.id = :id AND p.user.id = :userId", Boolean.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

}
