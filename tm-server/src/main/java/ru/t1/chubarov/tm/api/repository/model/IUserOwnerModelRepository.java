package ru.t1.chubarov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.dto.IDtoRepository;
import ru.t1.chubarov.tm.dto.model.AbstractUserOwnerModelDTO;
import ru.t1.chubarov.tm.model.AbstractUserOwnerModel;

public interface IUserOwnerModelRepository<M extends AbstractUserOwnerModel> extends IModelRepository<M> {

    @Nullable
    M findAllByUser(@NotNull String userId) throws Exception;

    @Nullable
    M findOneByIdByUser(@NotNull String userId, @Nullable String id) throws Exception;

    void remove(@NotNull String userId, @NotNull  M model) throws Exception;

    void removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    void add(@Nullable String userId, @Nullable M model);

    int getSizeByUser(@Nullable String userId) throws Exception;

    @NotNull
    Boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;


}
