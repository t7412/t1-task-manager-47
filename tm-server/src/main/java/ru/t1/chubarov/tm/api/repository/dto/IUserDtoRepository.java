package ru.t1.chubarov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserDtoRepository extends IDtoRepository<UserDTO> {

    @Nullable
    List<UserDTO> findAll();

    @Nullable
    UserDTO findOneById(@NotNull String id);

    void clear();

    void remove(@NotNull UserDTO model);

    int getSize();

    @Nullable
    UserDTO findByLogin(@NotNull String id);

    @Nullable
    UserDTO findByEmail(@NotNull String id);

    @NotNull
    Boolean isLoginExist(@NotNull String login);

    @NotNull
    Boolean isEmailExist(@NotNull String mail);

}
