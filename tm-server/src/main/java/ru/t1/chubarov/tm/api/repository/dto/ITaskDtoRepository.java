package ru.t1.chubarov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDtoRepository extends IDtoRepository<TaskDTO>{

    @Nullable
    List<TaskDTO> findAll();

    @Nullable
    List<TaskDTO> findAllByUser(@Nullable String userId);

    @Nullable
    TaskDTO findOneById(@NotNull String id);

    @Nullable
    TaskDTO findOneByIdByUser(@Nullable String userId, @Nullable String id);

    void clear();

    void removeAll(@Nullable String userId);

    void remove(@Nullable String userId, @NotNull TaskDTO model);

    void removeOneById(@Nullable String userId, @Nullable String id);

    int getSize();

    int getSizeByUser(@Nullable String userId);

    @NotNull
    Boolean existsById(@Nullable String userId, @Nullable String id);

}
